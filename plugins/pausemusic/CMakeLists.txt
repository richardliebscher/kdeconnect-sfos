kdeconnect_add_plugin(kdeconnect_pausemusic)

if(WIN32)
    target_sources(kdeconnect_pausemusic PRIVATE pausemusicplugin-win.cpp)
else()
    target_sources(kdeconnect_pausemusic PRIVATE pausemusicplugin.cpp)
endif()

target_link_libraries(kdeconnect_pausemusic
    kdeconnectcore
    kdeconnectinterfaces
    Qt5::Core
    Qt5::DBus
)
if (WIN32)
    target_link_libraries(kdeconnect_pausemusic windowsapp)
else()
    target_link_libraries(kdeconnect_pausemusic KF5::PulseAudioQt)
endif()

#######################################
# Config

if(NOT SAILFISHOS)
    kdeconnect_add_kcm(kdeconnect_pausemusic_config SOURCES pausemusic_config.cpp)

    ki18n_wrap_ui(kdeconnect_pausemusic_config pausemusic_config.ui)
    target_link_libraries(kdeconnect_pausemusic_config
        kdeconnectcore
        kdeconnectpluginkcm
        KF5::I18n
        KF5::KCMUtils
    )
endif()