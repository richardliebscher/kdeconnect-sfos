# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Marce Villarino <mvillarino@kde-espana.org>, 2013.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2015, 2016.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2017, 2018, 2019, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-22 00:41+0000\n"
"PO-Revision-Date: 2023-06-11 07:52+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.2\n"

#: package/contents/ui/Battery.qml:25
#, kde-format
msgid "%1% charging"
msgstr "%1% de carga"

#: package/contents/ui/Battery.qml:25
#, kde-format
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/Battery.qml:25
#, kde-format
msgid "No info"
msgstr "Sen información"

#: package/contents/ui/Connectivity.qml:40
#, kde-format
msgid "Unknown"
msgstr "Descoñecida"

#: package/contents/ui/Connectivity.qml:50
#, kde-format
msgid "No signal"
msgstr "Sen sinal"

#: package/contents/ui/DeviceDelegate.qml:56
#, kde-format
msgid "File Transfer"
msgstr "Transferencia de ficheiros"

#: package/contents/ui/DeviceDelegate.qml:57
#, kde-format
msgid "Drop a file to transfer it onto your phone."
msgstr "Solte un ficheiro para transferilo ao teléfono."

#: package/contents/ui/DeviceDelegate.qml:93
#, kde-format
msgid "Virtual Display"
msgstr "Pantalla virtual"

#: package/contents/ui/DeviceDelegate.qml:146
#, kde-format
msgctxt "Battery charge percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/DeviceDelegate.qml:168
#, kde-format
msgid "Please choose a file"
msgstr "Escolla un ficheiro"

#: package/contents/ui/DeviceDelegate.qml:177
#, kde-format
msgid "Share file"
msgstr "Compartir o ficheiro"

#: package/contents/ui/DeviceDelegate.qml:192
#, kde-format
msgid "Send Clipboard"
msgstr "Enviar o portapapeis"

#: package/contents/ui/DeviceDelegate.qml:211
#, kde-format
msgid "Ring my phone"
msgstr "Facer soar o meu móbil"

#: package/contents/ui/DeviceDelegate.qml:229
#, kde-format
msgid "Browse this device"
msgstr "Explorar este dispositivo"

#: package/contents/ui/DeviceDelegate.qml:246
#, kde-format
msgid "SMS Messages"
msgstr "Mensaxes SMS"

#: package/contents/ui/DeviceDelegate.qml:267
#, kde-format
msgid "Remote Keyboard"
msgstr "Teclado remoto"

#: package/contents/ui/DeviceDelegate.qml:288
#, kde-format
msgid "Notifications:"
msgstr "Notificacións:"

#: package/contents/ui/DeviceDelegate.qml:296
#, kde-format
msgid "Dismiss all notifications"
msgstr "Descartar todas as notificacións"

#: package/contents/ui/DeviceDelegate.qml:343
#, kde-format
msgid "Reply"
msgstr "Responder"

#: package/contents/ui/DeviceDelegate.qml:353
#, kde-format
msgid "Dismiss"
msgstr "Descartar"

#: package/contents/ui/DeviceDelegate.qml:366
#, kde-format
msgid "Cancel"
msgstr "Cancelar"

#: package/contents/ui/DeviceDelegate.qml:380
#, kde-format
msgctxt "@info:placeholder"
msgid "Reply to %1…"
msgstr "Responder a %1…"

#: package/contents/ui/DeviceDelegate.qml:398
#, kde-format
msgid "Send"
msgstr "Enviar"

#: package/contents/ui/DeviceDelegate.qml:424
#, kde-format
msgid "Run command"
msgstr "Executar unha orde"

#: package/contents/ui/DeviceDelegate.qml:432
#, kde-format
msgid "Add command"
msgstr "Engadir unha orde"

#: package/contents/ui/FullRepresentation.qml:58
#, kde-format
msgid "No paired devices"
msgstr "Non hai dispositivos emparellados"

#: package/contents/ui/FullRepresentation.qml:58
#, kde-format
msgid "Paired device is unavailable"
msgid_plural "All paired devices are unavailable"
msgstr[0] "O dispositivo emparellado non está dispoñíbel"
msgstr[1] "Non hai ningún dispositivo emparellado dispoñíbel"

#: package/contents/ui/FullRepresentation.qml:60
#, kde-format
msgid "Install KDE Connect on your Android device to integrate it with Plasma!"
msgstr ""
"Instale KDE Connect no seu dispositivo con Android para integralo con Plasma!"

#: package/contents/ui/FullRepresentation.qml:64
#, kde-format
msgid "Pair a Device..."
msgstr "Emparellar cun dispositivo…"

#: package/contents/ui/FullRepresentation.qml:76
#, kde-format
msgid "Install from Google Play"
msgstr "Instalar desde Google Play"

#: package/contents/ui/FullRepresentation.qml:86
#, kde-format
msgid "Install from F-Droid"
msgstr "Instalar desde F-Droid"

#: package/contents/ui/main.qml:59
#, kde-format
msgid "KDE Connect Settings..."
msgstr "Configuración de KDE Connect…"

#~ msgid "Save As"
#~ msgstr "Gardar como"

#~ msgid "Take a photo"
#~ msgstr "Sacar unha foto"

#, fuzzy
#~| msgid "Battery"
#~ msgctxt ""
#~ "Display the battery charge percentage with the label \"Battery:\" so the "
#~ "user knows what is being displayed"
#~ msgid "Battery: %1"
#~ msgstr "Batería"

#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"

#~ msgid "Share text"
#~ msgstr "Compartir o texto"

#~ msgid "Charging: %1%"
#~ msgstr "Cargándose: %1%"

#~ msgid "Discharging: %1%"
#~ msgstr "Descargándose: %1%"
