/*
 * SPDX-FileCopyrightText: 2013 Albert Vaca <albertvaka@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

#include "uploadjob.h"

#include <KLocalizedString>

#include "core_debug.h"
#include "kdeconnectconfig.h"
#include "lanlinkprovider.h"
#include <daemon.h>
#include "qtcompat_p.h"

UploadJob::UploadJob(const NetworkPacket &networkPacket)
    : KJob()
    , m_networkPacket(networkPacket)
    , m_input(networkPacket.payload())
    , m_socket(nullptr)
    , m_bufferStart(0)
    , m_bufferEnd(0)
{
}

void UploadJob::setSocket(QSslSocket *socket)
{
    m_socket = socket;
    m_socket->setParent(this);
}

void UploadJob::start()
{
    if (!m_input->open(QIODevice::ReadOnly)) {
        qCWarning(KDECONNECT_CORE) << "error when opening the input to upload";
        return; // TODO: Handle error, clean up...
    }

    if (!m_socket) {
        qCWarning(KDECONNECT_CORE) << "you must call setSocket() before calling start()";
        return;
    }

    bytesUploaded = 0;
    setProcessedAmount(Bytes, bytesUploaded);

    connect(m_socket, &QSslSocket::encryptedBytesWritten, this, &UploadJob::encryptedBytesWritten);
    connect(m_socket, &QAbstractSocket::disconnected, this, &UploadJob::onDisconnected);
    uploadNextPacket();
}

void UploadJob::uploadNextPacket()
{
    size_t bufferLen = m_bufferEnd - m_bufferStart;
    qint64 bytesAvailable = m_input->bytesAvailable() + bufferLen;

    if (bufferLen == 0 && bytesAvailable > 0) {
        qint64 bytesToSend = qMin(bytesAvailable, (qint64)m_buffer.size());
        qint64 bytesRead = m_input->read(m_buffer.data(), bytesToSend);
        if (bytesRead < 0) {
            stop();
            setError(KJob::UserDefinedError);
            setErrorText(QStringLiteral("Failed to read file: ") + m_input->errorString());
            emitResult();
            return;
        }
        m_bufferStart = 0;
        m_bufferEnd = bytesRead;
        bufferLen = bytesRead;
    }

    if (bufferLen > 0) {
        bytesUploading = m_socket->write(m_buffer.data() + m_bufferStart, bufferLen);
        if (bytesUploading < 0) {
            stop();
            setError(KJob::UserDefinedError);
            setErrorText(QStringLiteral("Failed to upload: ") + m_socket->errorString());
            emitResult();
            return;
        }
        m_bufferStart += bytesUploading;
    }

    if (bytesAvailable == 0 || bytesUploading <= 0) {
        m_input->close();
        m_socket->disconnectFromHost();
        disconnect(m_socket, &QSslSocket::encryptedBytesWritten, this, &UploadJob::encryptedBytesWritten);
    }
}

void UploadJob::encryptedBytesWritten(qint64 /*bytes*/)
{
    if (m_socket->encryptedBytesToWrite() == 0) {
        bytesUploaded += bytesUploading;
        setProcessedAmount(Bytes, bytesUploaded);

        uploadNextPacket();
    }
}

void UploadJob::onDisconnected()
{
    emitResult();
}

bool UploadJob::stop()
{
    m_input->close();
    m_socket->close();

    disconnect(m_socket, &QSslSocket::encryptedBytesWritten, this, &UploadJob::encryptedBytesWritten);
    disconnect(m_socket, &QAbstractSocket::disconnected, this, &UploadJob::onDisconnected);

    return true;
}

const NetworkPacket UploadJob::getNetworkPacket()
{
    return m_networkPacket;
}

#include "moc_uploadjob.cpp"
