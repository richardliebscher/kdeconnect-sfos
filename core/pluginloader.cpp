/**
 * SPDX-FileCopyrightText: 2013 Albert Vaca <albertvaka@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

#include "pluginloader.h"

#include <KPluginFactory>
#include <KPluginMetaData>
#include <QPluginLoader>
#include <QStaticPlugin>
#include <QVector>

#include "core_debug.h"
#include "device.h"
#include "kdeconnectconfig.h"
#include "kdeconnectplugin.h"

PluginLoader *PluginLoader::instance()
{
    static PluginLoader *instance = new PluginLoader();
    return instance;
}

PluginLoader::PluginLoader()
{
    const QVector<QStaticPlugin> staticPlugins = QPluginLoader::staticPlugins();
    for (auto& staticPlugin : staticPlugins) {
        QJsonObject pluginMetadata = staticPlugin.metaData();
        QJsonObject jsonMetadata = pluginMetadata.value(QStringLiteral("MetaData")).toObject();
        if (jsonMetadata.contains(QStringLiteral("X-KdeConnect-OutgoingPacketType"))
                || jsonMetadata.contains(QStringLiteral("X-KdeConnect-SupportedPacketType"))) {
            QString pluginId = pluginMetadata.value(QStringLiteral("className")).toString();
            pluginId.chop(7 + 6);

            auto kplugin = jsonMetadata.value(QStringLiteral("KPlugin")).toObject();
            kplugin.insert(QStringLiteral("Id"), pluginId);
            jsonMetadata.insert(QStringLiteral("KPlugin"), kplugin);

            plugins.insert(pluginId, KPluginMetaData(jsonMetadata, QString()));
            pluginsFactories.insert(
                pluginId,
                qobject_cast<KPluginFactory*>(staticPlugin.instance()));
        }
    }
}

QStringList PluginLoader::getPluginList() const
{
    return plugins.keys();
}

bool PluginLoader::doesPluginExist(const QString &name) const
{
    return plugins.contains(name);
}

KPluginMetaData PluginLoader::getPluginInfo(const QString &name) const
{
    return plugins.value(name);
}

KdeConnectPlugin *PluginLoader::instantiatePluginForDevice(const QString &pluginName, Device *device) const
{
    const KPluginMetaData data = plugins.value(pluginName);
    if (!data.isValid()) {
        qCDebug(KDECONNECT_CORE) << "Plugin unknown" << pluginName;
        return nullptr;
    }

    const QStringList outgoingInterfaces = KPluginMetaData::readStringList(data.rawData(), QStringLiteral("X-KdeConnect-OutgoingPacketType"));
    const QVariantList args{QVariant::fromValue<Device *>(device), pluginName, outgoingInterfaces, data.iconName()};

    KPluginFactory* factory = pluginsFactories.value(pluginName);
    if (auto result = factory->create<KdeConnectPlugin>(device, args)) {
        qCDebug(KDECONNECT_CORE) << "Loaded plugin:" << data.pluginId();
        return result;
    } else {
        qCDebug(KDECONNECT_CORE) << "Error loading plugin";
        return nullptr;
    }
}

QStringList PluginLoader::incomingCapabilities() const
{
    QSet<QString> ret;
    for (const KPluginMetaData &service : plugins) {
        ret += KPluginMetaData::readStringList(service.rawData(), QStringLiteral("X-KdeConnect-SupportedPacketType")).toSet();
    }
    return ret.values();
}

QStringList PluginLoader::outgoingCapabilities() const
{
    QSet<QString> ret;
    for (const KPluginMetaData &service : plugins) {
        ret += KPluginMetaData::readStringList(service.rawData(), QStringLiteral("X-KdeConnect-OutgoingPacketType")).toSet();
    }
    return ret.values();
}

QSet<QString> PluginLoader::pluginsForCapabilities(const QSet<QString> &incoming, const QSet<QString> &outgoing) const
{
    QSet<QString> ret;

    QString myDeviceType = KdeConnectConfig::instance().deviceType().toString();

    for (const KPluginMetaData &service : plugins) {
        // Check if the plugin support this device type
        const QStringList supportedDeviceTypes = service.rawData().value(QStringLiteral("X-KdeConnect-SupportedDeviceTypes")).toVariant().toStringList();
        if (!supportedDeviceTypes.isEmpty()) {
            if (!supportedDeviceTypes.contains(myDeviceType)) {
                qCDebug(KDECONNECT_CORE) << "Not loading plugin" << service.pluginId() << "because this device of type" << myDeviceType
                                         << "is not supported. Supports:" << supportedDeviceTypes.join(QStringLiteral(", "));
                continue;
            }
        }

        // Check if capbilites intersect with the remote device
        const QStringList pluginIncomingCapabilities = service.rawData().value(QStringLiteral("X-KdeConnect-SupportedPacketType")).toVariant().toStringList();
        const QStringList pluginOutgoingCapabilities = service.rawData().value(QStringLiteral("X-KdeConnect-OutgoingPacketType")).toVariant().toStringList();

        bool capabilitiesEmpty = (pluginIncomingCapabilities.isEmpty() && pluginOutgoingCapabilities.isEmpty());
        if (!capabilitiesEmpty) {
            QSet<QString> commonIncoming = incoming;
            commonIncoming.intersect(pluginOutgoingCapabilities.toSet());
            QSet<QString> commonOutgoing = outgoing;
            commonOutgoing.intersect(pluginIncomingCapabilities.toSet());
            bool capabilitiesIntersect = (!commonIncoming.isEmpty() || !commonOutgoing.isEmpty());
            if (!capabilitiesIntersect) {
                qCDebug(KDECONNECT_CORE) << "Not loading plugin" << service.pluginId() << "because device doesn't support it";
                continue;
            }
        }

        // If we get here, the plugin can be loaded
        ret += service.pluginId();
    }
qCDebug(KDECONNECT_CORE) << "Loaded" << ret << "for"<< incoming << outgoing;
    return ret;
}
